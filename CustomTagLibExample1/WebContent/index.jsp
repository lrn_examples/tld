<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://codeuniverse.ru/myprojectname/taglibs/numberformatter" prefix="t"%>
<%@ taglib uri="anyServerUniqStringUsuallyHttpSth" prefix="mypref" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>


<t:formatNumber numberToFormat="123.345" formatString="#.##"/>

<br>

<mypref:myoutput myAttr1="my text"/>

<% String s = "7"; %>
<br>
<mypref:myoutput myAttr1="<%= s %>"/>

</body>
</html>