package com.my.jsp.customtags;

import java.io.IOException;
import java.text.DecimalFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.SkipPageException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

// (Custom) Tag Handler class
public class MyNumberFormatterTag extends SimpleTagSupport {

	//  could be double - custom tag attribute is automatically converted
	// Double.valueOf(String). Same - to all Wrappers. 
	// but for <my:customtag attr="<%= jsp_expr %>" /> no such conversion  
	// => type should 100% match
	private String numberToFormat;

	private String formatString; // formatting pattern

	// default version has super.doTag(); // does nothing
	@Override
	public void doTag() throws JspException, IOException {
		try {
			double numberToFormatAsDouble = Double.parseDouble(numberToFormat);
			DecimalFormat formatter = new DecimalFormat(formatString);
			String formattedNumber = formatter.format(numberToFormatAsDouble);

			// get JspWriter class - getJspContext().getOut()
			// then write String
			
			// getJspContext() returns JspContext
			// PageContext is child of JspContext
			PageContext pageContext = (PageContext) getJspContext();
			JspWriter jspWriter = pageContext.getOut();
			jspWriter.write(formattedNumber);
			// jspWriter.print(formattedNumber);   // alternative!
			// jspWriter.println(formattedNumber); // alternative!
			
		} catch (Exception e) {
			// stop page from loading further
			throw new SkipPageException("ArithmeticException while formatting number " + numberToFormat
					+ " with formatString " + formatString);
		}

	}

	

	public void setNumberToFormat(String numberToFormat) {
		this.numberToFormat = numberToFormat;
	}
	
	public void setFormatString(String formatString) {
		this.formatString = formatString;
	}

}
